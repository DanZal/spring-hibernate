package com.noroff.HibernatePostgreSQLAssignment.controllers;

import com.noroff.HibernatePostgreSQLAssignment.Pojo.FranchiseRequest;
import com.noroff.HibernatePostgreSQLAssignment.Pojo.MovieRequest;
import com.noroff.HibernatePostgreSQLAssignment.models.Franchise;
import com.noroff.HibernatePostgreSQLAssignment.models.Movies;
import com.noroff.HibernatePostgreSQLAssignment.services.FranchiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class FranchiseController {

    @Autowired
    FranchiseService franchiseService;

    //Gets all the franchises
    @GetMapping("get/franchises")
    public List<Franchise> getMovies() {
        return franchiseService.getFranchise();
    }

    //Gets one franchise by id
    @GetMapping("get/franchise/{id}")
    public Franchise getMovieById(@PathVariable(value = "id") long franchiseId) {
        return franchiseService.getFranchiseById(franchiseId);
    }

    //Update franchise by id
    @PutMapping("update/franchise/{id}")
    public Franchise updateFranchiseById(@PathVariable(value = "id") long franchiseId, @RequestBody FranchiseRequest franchiseRequest) {
        return franchiseService.updateFranchiseById(franchiseId, franchiseRequest);

    }

    //Update movies inside franchise with franchiseId
    @PutMapping("/update/franchisemovie/{id}")
    public Franchise updateFranchiseMovieById(@PathVariable(value = "id") long franchiseId, @RequestBody FranchiseRequest franchiseRequest) {
        return franchiseService.updateFranchiseMovieById(franchiseId, franchiseRequest);

    }

    //Add a franchise
    @PostMapping("/add/franchise")
    public Franchise AddFranchise(@RequestBody Franchise franchise){
        return franchiseService.addFranchise(franchise);
    }

    //Add a franchise with movies
    @PostMapping("/add/franchisemovie")
    public Franchise addFranchiseWithMovies(@RequestBody FranchiseRequest franchiseRequest) {
        return franchiseService.addFranchiseWithMovies(franchiseRequest);
    }



}
