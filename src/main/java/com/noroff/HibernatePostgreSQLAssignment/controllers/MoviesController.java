package com.noroff.HibernatePostgreSQLAssignment.controllers;

import com.noroff.HibernatePostgreSQLAssignment.Pojo.MovieRequest;
import com.noroff.HibernatePostgreSQLAssignment.models.Movies;
import com.noroff.HibernatePostgreSQLAssignment.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class MoviesController {

    @Autowired
    MovieService movieService;

    /*@GetMapping("api/getfranchise")
    public Franchise*/

    @GetMapping("movies")
    public String printMovies(){
        return "Movies";
    }

    //Get all movies
    @GetMapping("get/movies")
    public List<Movies> getMovies() {
        return movieService.getMovies();
    }

    //Get movie by id
    @GetMapping("get/movie/{id}")
    public Movies getMovieById(@PathVariable(value = "id") long movieId) {
        return movieService.getMovieById(movieId);
    }


    //Add movie
    @PostMapping("add/movie")
    public Movies AddMovie(@RequestBody Movies movies){
        return movieService.addMovie(movies);
    }

    //Get movie by title
    @GetMapping("get/movietitle")
    public Movies getMovie(String title){
        return movieService.getMovie(title);
    }

    //Add movie with requestbody
    @PostMapping("save/movie")
    public Movies saveMovie(@RequestBody MovieRequest movieRequest) {
        return movieService.saveMovie(movieRequest);
    }

    //Add movie with characters
    @PostMapping("add/moviecharacter")
    public Movies addMovieWithCharacters(@RequestBody MovieRequest movieRequest) {
        return movieService.addMovieWithCharacters(movieRequest);
    }

    //Delete a movie
    @DeleteMapping("delete/movie/{id}")
    public String deleteMovieById(@PathVariable(value = "id") long movieId) {
        movieService.deleteByID(movieId);
        return "Movie deleted with id: " + movieId;
    }

    //Update movie by id
    @PutMapping("update/movie/{id}")
    public Movies updateMovieById(@PathVariable(value = "id") long movieId, @RequestBody MovieRequest movieRequest) {
        return movieService.updateMovieById(movieId, movieRequest);

    }

    //Update characters in a movie by id
    @PutMapping("update/moviecharacter/{id}")
    public Movies updateMovieCharacterById(@PathVariable(value = "id") long movieId, @RequestBody MovieRequest movieRequest) {
        return movieService.updateMovieCharacterById(movieId, movieRequest);

    }


}
