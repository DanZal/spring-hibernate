package com.noroff.HibernatePostgreSQLAssignment.controllers;

import com.noroff.HibernatePostgreSQLAssignment.Pojo.CharacterRequest;
import com.noroff.HibernatePostgreSQLAssignment.Pojo.MovieRequest;
import com.noroff.HibernatePostgreSQLAssignment.models.Character;
import com.noroff.HibernatePostgreSQLAssignment.models.Franchise;
import com.noroff.HibernatePostgreSQLAssignment.models.Movies;
import com.noroff.HibernatePostgreSQLAssignment.repos.CharacterRepository;
import com.noroff.HibernatePostgreSQLAssignment.services.CharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CharacterController {

    @Autowired
    CharacterService characterService;

    //Get all characters
    @GetMapping("/get/characters")
    public List<Character> getCharacter() {
        return characterService.getCharacters();
    }

    //Add a character
    @PostMapping("/add/character")
    public Character AddCharacter(@RequestBody Character character){
        return characterService.addCharacter(character);
    }

    //Add a movie and a character
    @PostMapping("/add/charactermovies")
    public Character AddCharacterMovie(@RequestBody CharacterRequest characterRequest){
        return characterService.addCharacterMovie(characterRequest);
    }

    //Update characters by character id
    @PutMapping("/update/character/{id}")
    public Character updateCharacterById(@PathVariable(value = "id") long characterId, @RequestBody CharacterRequest characterRequest) {
        return characterService.updateCharacterById(characterId, characterRequest);

    }

    //Delete character by id without deleting parent entities
    @DeleteMapping("delete/character/{id}")
    public String deleteCharacterById(@PathVariable(value = "id") long characterId) {
        characterService.deleteByID(characterId);
        return "Character deleted with id: " + characterId;
    }
}
