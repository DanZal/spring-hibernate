package com.noroff.HibernatePostgreSQLAssignment;

import com.noroff.HibernatePostgreSQLAssignment.models.Character;
import com.noroff.HibernatePostgreSQLAssignment.models.Franchise;
import com.noroff.HibernatePostgreSQLAssignment.models.Movies;
import com.noroff.HibernatePostgreSQLAssignment.repos.CharacterRepository;
import com.noroff.HibernatePostgreSQLAssignment.repos.FranchiseRepository;
import com.noroff.HibernatePostgreSQLAssignment.repos.MoviesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;


@Component
public class Seeder implements ApplicationRunner {

    Franchise mcu = new Franchise(
            "Marvel cinematic universe",
            "The MCU includes all marvel movies"
    );

    Franchise bond = new Franchise(
            "James Bond",
            "All the james bond johnsons"
    );

    Character tommy = new Character(
            "Tommy Shelby",
            "shelbs",
            "male",
            "www.pic.com/shelby"
    );

    Character hulk = new Character(
            "Bruce Banner",
            "Hulk",
            "male",
            "www.pic.com/hulk"
    );

    Character agent = new Character(
            "James Bond",
            "007",
            "male",
            "pic.com/007"
    );

    Character bgirl = new Character(
            "Paloma",
            "none",
            "female",
            "www.pic.com/paloma"
    );



    @Autowired
    MoviesRepository moviesRepository;
    @Autowired
    FranchiseRepository franchiseRepository;
    @Autowired
    CharacterRepository characterRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("From application runner");
        loadFranchise();
        loadMovies();
        loadCharacters();
        characterRepository.save(agent);
        characterRepository.save(bgirl);


    }

    private void loadMovies() {
        Movies scarface = new Movies("Scarface",
                "Crime",
                1983,
                "Brian de Palma",
                "http://picture.com/scarface",
                "http://trailer.com/scarface");
        Movies thor = new Movies(
                "Thor Ragnarok",
                "Action",
                2017,
                "Taika Waititi",
                "http://picture.com/thor",
                "http://trailer.com/thor");
        Movies avenger = new Movies(
                "The Avengers",
                "Action",
                2012,
                "Joss Whedon",
                "http://picture.com/avengers",
                "http://trailer.com/avengers");
        Movies timeToDie = new Movies(
                "No Time to Die",
                "Action",
                2021,
                "Cary Joji Fukunaga",
                "http://picture.com/bond",
                "http://trailer.com/bond");

        thor.setFranchise(mcu);
        avenger.setFranchise(mcu);
        avenger.addCharacter(hulk);
        timeToDie.addCharacter(agent);
        timeToDie.setFranchise(bond);


        if (moviesRepository.count() == 0) {

            moviesRepository.save(scarface);
            moviesRepository.save(thor);
            moviesRepository.save(avenger);
            moviesRepository.save(timeToDie);


        }
        System.out.println(moviesRepository.count());
    }

    private void loadFranchise() {
        if (franchiseRepository.count() == 0) {
            franchiseRepository.save(mcu);
            franchiseRepository.save(bond);
        }
        System.out.println(franchiseRepository.count());
    }

    private void loadCharacters() {
        if (characterRepository.count() == 0) {
            characterRepository.save(hulk);
            characterRepository.save(tommy);

        }
    }

}
