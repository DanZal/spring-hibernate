package com.noroff.HibernatePostgreSQLAssignment.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "Character")
public class Character {

    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private long characterId;
    private String name;
    private String alias;
    private String gender;
    private String pictureUrl;

    public Character(String name, String alias, String gender, String pictureUrl) {
        this.name = name;
        this.alias = alias;
        this.gender = gender;
        this.pictureUrl = pictureUrl;
    }

    public Character() {

    }

    @ManyToMany(mappedBy = "character", cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JsonIgnore
    private Set<Movies> movies = new HashSet<>();

    public long getCharacterId() {
        return characterId;
    }

    public void setCharacterId(long characterId) {
        this.characterId = characterId;
    }

    public Set<Movies> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movies> movies) {
        this.movies = movies;
    }

    public void addMovie(Movies movie) {
        this.movies.add(movie);
    }

    public long getId() {
        return characterId;
    }

    public void setId(long characterId) {
        this.characterId = characterId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }
}
