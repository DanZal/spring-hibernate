package com.noroff.HibernatePostgreSQLAssignment.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Set;

@Entity (name = "Franchise")
public class Franchise {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "franchise_id")
    private long franchiseId;
    private String name;
    private String description;

    @OneToMany(mappedBy = "franchise")
    private Set<Movies> movies;

    public Franchise(String name, String description) {

        this.name = name;
        this.description = description;
    }

    public Franchise() {

    }

    public Set<Movies> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movies> movies) {
        this.movies = movies;
    }

    public long getId() {
        return franchiseId;
    }

    public void setId(long franchiseId) {
        this.franchiseId = franchiseId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
