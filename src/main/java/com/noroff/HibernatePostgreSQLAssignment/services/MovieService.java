package com.noroff.HibernatePostgreSQLAssignment.services;

import com.noroff.HibernatePostgreSQLAssignment.Pojo.MovieRequest;
import com.noroff.HibernatePostgreSQLAssignment.models.Character;
import com.noroff.HibernatePostgreSQLAssignment.models.Franchise;
import com.noroff.HibernatePostgreSQLAssignment.models.Movies;
import com.noroff.HibernatePostgreSQLAssignment.repos.CharacterRepository;
import com.noroff.HibernatePostgreSQLAssignment.repos.FranchiseRepository;
import com.noroff.HibernatePostgreSQLAssignment.repos.MoviesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MovieService {

    @Autowired
    MoviesRepository moviesRepository;
    @Autowired
    FranchiseRepository franchiseRepository;
    @Autowired
    CharacterRepository characterRepository;

    public MovieService() {

    }

    public List<Movies> getMovies() {

        return moviesRepository.findAll();
    }

  /*  public Movies updateMovie(long id) {
        Optional<Movies> movie = moviesRepository.findById(id);
        movie.
    } */

    public Movies addMovie(Movies movies) {
        return moviesRepository.save(movies);
    }

    public Movies getMovie(String title) {
        return moviesRepository.findByTitle(title);
    }

    public Movies getMovieById(long movieId) {return moviesRepository.findById(movieId);}

    public Movies saveMovie(MovieRequest movieRequest) {
        //TODO: move the franchise to franchise service and call the method
            Franchise franchise = franchiseRepository.findById(movieRequest.franchise_id);
            Movies movies = new Movies();
            movies.setTitle(movieRequest.title);
            movies.setGenre(movieRequest.genre);
            movies.setReleaseYear(movieRequest.releaseYear);
            movies.setDirector(movieRequest.director);
            movies.setPictureUrl(movieRequest.pictureUrl);
            movies.setTrailerUrl(movieRequest.trailerUrl);
            movies.setFranchise(franchise);

            return moviesRepository.save(movies);

    }

    public Movies addMovieWithCharacters(MovieRequest movieRequest){
        Movies movie = new Movies();
        Franchise franchise = franchiseRepository.findById(movieRequest.franchise_id);
        //TODO: Add franchise and setFranchise method
        movie.setTitle(movieRequest.title);
        movie.setGenre(movieRequest.genre);
        movie.setReleaseYear(movieRequest.releaseYear);
        movie.setDirector(movieRequest.director);
        movie.setPictureUrl(movieRequest.pictureUrl);
        movie.setTrailerUrl(movieRequest.trailerUrl);
        movie.setFranchise(franchise);
        characterSetMethod(movieRequest, movie);
        return moviesRepository.save(movie);

    }

    public void deleteByID(long movieId) {
        Movies movie = moviesRepository.findById(movieId);
        moviesRepository.delete(movie);
     }

    public Movies updateMovieById(long movieId, MovieRequest movieRequest) {
        Franchise franchise = franchiseRepository.findById(movieRequest.franchise_id);
        Movies movie = moviesRepository.findById(movieId);
        movie.setTitle(movieRequest.title);
        movie.setGenre(movieRequest.genre);
        movie.setReleaseYear(movieRequest.releaseYear);
        movie.setDirector(movieRequest.director);
        movie.setPictureUrl(movieRequest.pictureUrl);
        movie.setTrailerUrl(movieRequest.trailerUrl);
        movie.setFranchise(franchise);
        characterSetMethod(movieRequest, movie);
        return moviesRepository.save(movie);

    }

    public Movies updateMovieCharacterById(long movieId, MovieRequest movieRequest) {
        Movies movie = moviesRepository.findById(movieId);
        characterSetMethod(movieRequest, movie);
        return moviesRepository.save(movie);

    }

    private void characterSetMethod(MovieRequest movieRequest, Movies movie) {
        movie.setCharacter(movieRequest.characters.stream().map(
                character -> {
                    Character ccharacter = character;
                    if (ccharacter.getCharacterId() > 0) {
                        ccharacter = characterRepository.findById(ccharacter.getCharacterId());
                    }
                    ccharacter.addMovie(movie);
                    return ccharacter;
                }
        ).collect(Collectors.toSet()));
    }



}
