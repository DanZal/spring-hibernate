package com.noroff.HibernatePostgreSQLAssignment.services;

import com.noroff.HibernatePostgreSQLAssignment.Pojo.CharacterRequest;
import com.noroff.HibernatePostgreSQLAssignment.models.Character;
import com.noroff.HibernatePostgreSQLAssignment.models.Franchise;
import com.noroff.HibernatePostgreSQLAssignment.models.Movies;
import com.noroff.HibernatePostgreSQLAssignment.repos.CharacterRepository;
import com.noroff.HibernatePostgreSQLAssignment.repos.MoviesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CharacterService {

    @Autowired
    CharacterRepository characterRepository;

    @Autowired
    MoviesRepository moviesRepository;

    public CharacterService() {

    }


    public List<Character> getCharacters() {

       return characterRepository.findAll();
    }

    public Character addCharacter(Character character) {
        return characterRepository.save(character);
    }

    public Character addCharacterMovie(CharacterRequest characterRequest) {

        Character character = new Character();
        character.setName(characterRequest.characterName);
        character.setAlias(characterRequest.alias);
        character.setGender(characterRequest.gender);
        character.setPictureUrl(characterRequest.pictureUrl);
        character.setMovies(characterRequest.movies.stream().map(
                movies -> {
                    Movies mmovies = movies;
                    if (mmovies.getMovieId() > 0) {
                        mmovies = moviesRepository.findById(mmovies.getMovieId());
                    }
                    mmovies.addCharacter(character);
                    return mmovies;
                }
        ).collect(Collectors.toSet()));

        return characterRepository.save(character);
    }

    public Character updateCharacterById(long characterId, CharacterRequest characterRequest) {

        Character character = characterRepository.findById(characterId);
        character.setName(characterRequest.characterName);
        character.setAlias(characterRequest.alias);
        character.setGender(characterRequest.gender);
        character.setPictureUrl(characterRequest.pictureUrl);
        return characterRepository.save(character);
    }

    public void deleteByID(long characterId) {
        Character character = characterRepository.findById(characterId);

        //Deletes the relation in movies_character to ensure owning entity is not deleted
        for (Movies m : character.getMovies()) {
            if (m.getCharacter().size() == 1) {
                characterRepository.delete(character);
            } else {
                m.getCharacter().remove(character);
            }
        }
        characterRepository.delete(character);


    }


}
