package com.noroff.HibernatePostgreSQLAssignment.services;

import com.noroff.HibernatePostgreSQLAssignment.Pojo.FranchiseRequest;
import com.noroff.HibernatePostgreSQLAssignment.models.Franchise;
import com.noroff.HibernatePostgreSQLAssignment.models.Movies;
import com.noroff.HibernatePostgreSQLAssignment.repos.FranchiseRepository;
import com.noroff.HibernatePostgreSQLAssignment.repos.MoviesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class FranchiseService {

    @Autowired
    FranchiseRepository franchiseRepository;

    @Autowired
    MoviesRepository moviesRepository;

    public List<Franchise> getFranchise() {
        return franchiseRepository.findAll();
    }

    public Franchise getFranchiseById(long franchiseId) {

        return franchiseRepository.findById(franchiseId);
    }

    public Franchise updateFranchiseMovieById(long franhiseId, FranchiseRequest franchiseRequest) {

        Franchise franchise = franchiseRepository.findById(franhiseId);
        movieSetMethod(franchiseRequest, franchise);
        return franchiseRepository.save(franchise);
    }

    private void movieSetMethod(FranchiseRequest franchiseRequest, Franchise franchise) {
        franchise.setMovies(franchiseRequest.movies.stream().map(
                movie -> {
                    Movies mmovie = movie;
                    if (mmovie.getMovieId() > 0) {
                        mmovie = moviesRepository.findById(mmovie.getMovieId());
                    }
                    mmovie.setFranchise(franchise);
                    return mmovie;
                }
        ).collect(Collectors.toSet()));
    }

    public Franchise addFranchiseWithMovies(FranchiseRequest franchiseRequest) {

        Franchise franchise = new Franchise();
        franchise.setName(franchiseRequest.name);
        franchise.setDescription(franchiseRequest.description);
        if (franchiseRequest.movies != null) {
            movieSetMethod(franchiseRequest, franchise);
        }
        return franchiseRepository.save(franchise);
    }

    public Franchise addFranchise(Franchise franchise) {
        return franchiseRepository.save(franchise);
    }

    public Franchise updateFranchiseById(long franchiseId, FranchiseRequest franchiseRequest) {

        Franchise franchise = franchiseRepository.findById(franchiseId);
        franchise.setName(franchiseRequest.name);
        franchise.setDescription(franchiseRequest.description);

        return franchiseRepository.save(franchise);
    }


}
