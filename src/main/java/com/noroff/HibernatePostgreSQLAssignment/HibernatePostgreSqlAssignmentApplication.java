package com.noroff.HibernatePostgreSQLAssignment;

import com.noroff.HibernatePostgreSQLAssignment.models.Franchise;
import com.noroff.HibernatePostgreSQLAssignment.models.Movies;
import com.noroff.HibernatePostgreSQLAssignment.repos.FranchiseRepository;
import com.noroff.HibernatePostgreSQLAssignment.repos.MoviesRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class HibernatePostgreSqlAssignmentApplication {

	public static void main(String[] args) {

		SpringApplication.run(HibernatePostgreSqlAssignmentApplication.class, args);


	}





}
