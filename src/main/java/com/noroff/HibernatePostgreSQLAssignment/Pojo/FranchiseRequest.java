package com.noroff.HibernatePostgreSQLAssignment.Pojo;

import com.noroff.HibernatePostgreSQLAssignment.models.Movies;

import java.util.Set;

public class FranchiseRequest {

    public long franchiseId;
    public String name;
    public String description;
    public long movieId;
    public Set<Movies> movies;

}
