package com.noroff.HibernatePostgreSQLAssignment.Pojo;

import com.noroff.HibernatePostgreSQLAssignment.models.Character;

import java.util.Set;

public class MovieRequest {

    public long id;
    public String title;
    public String genre;
    public int releaseYear;
    public String director;
    public String pictureUrl;
    public String trailerUrl;
    public long franchise_id;
    public Set<Character> characters;



}
