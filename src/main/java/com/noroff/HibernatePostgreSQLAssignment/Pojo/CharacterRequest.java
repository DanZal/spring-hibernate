package com.noroff.HibernatePostgreSQLAssignment.Pojo;

import com.noroff.HibernatePostgreSQLAssignment.models.Movies;


import java.util.Set;

public class CharacterRequest {

    public String characterName;
    public String alias;
    public String gender;
    public String pictureUrl;
    public Set<Movies> movies;
}
