package com.noroff.HibernatePostgreSQLAssignment.repos;

import com.noroff.HibernatePostgreSQLAssignment.models.Movies;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MoviesRepository extends JpaRepository<Movies, Long> {

    Movies findByTitle(String title);
    Movies findById(long id);
}
