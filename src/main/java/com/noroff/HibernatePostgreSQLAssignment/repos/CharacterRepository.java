package com.noroff.HibernatePostgreSQLAssignment.repos;

import com.noroff.HibernatePostgreSQLAssignment.models.Character;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterRepository extends JpaRepository<Character, Long> {

     Character findById(long id);
}
