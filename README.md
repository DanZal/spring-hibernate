# spring-hibernate

Used hibernate to create a database with PostgreSQL.

# Models and relations in the project

Characters - can be in a movie

Movies - can be in a franchise and can have characters

Franchise - can have movies

# API Endpoints 

Can be accessed by http://localhost:8080/api-docs

Or http://localhost:8080/swagger-ui/index.html?configUrl=/api-docs/swagger-config#/



